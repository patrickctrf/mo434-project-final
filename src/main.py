import csv
import json

import PIL
import torch
import torchvision
from numpy import arange, array, random
from pandas import DataFrame
from skimage.metrics import mean_squared_error
from sklearn.metrics import make_scorer, roc_auc_score, confusion_matrix
from sklearn.model_selection import StratifiedShuffleSplit, GridSearchCV, train_test_split
from torch import nn, from_numpy, tensor, float32
from torch.nn import LeakyReLU, Linear, Sequential
from torch.utils.data import Subset, DataLoader
from torchvision.transforms import transforms
from tqdm import tqdm

from breakhis_dataset import BreakhisDataset
from breakhis_dataset_v2 import BreakhisDatasetV2
from utils import DataManager

MAGNIFYING_FACTORS = [40]


# from IPython.core.display import display
# from torch import nn, from_numpy, Tensor

class DiagModule(nn.Module):

    def __init__(self, epochs=150, validation_percent=0.2, output_features=1, device=torch.device("cpu"), data_augmentation=True, fine_tunning=False, resize_images=True):
        """
This class implements our cancer identification model, using a pretrained
imagenet model and transfer learning.

There is a fit() method to train this model according to the parameters given in
the class initialization. It follows the sklearn header pattern.

This is also an sklearn-like estimator and may be used with any sklearn method
designed for classical estimators. But, when using GPU as PyTorch device, you
CAN'T use multiple sklearn workers (n_jobs), beacuse it raises an serializtion
error within CUDA.

        :param epochs: The number of epochs to train. The final model after
        train will be the one with best VALIDATION loss, not necessarily the
        model found after whole "epochs" number.
        :param validation_percent: The percentage of samples reserved for
        validation (cross validation) during training inside fit() method.
        :param output_features: (int) Format output with correct features size.
        :param device: PyTorch device, such as torch.device("cpu") or
        torch.device("cuda:0").data_augmentation
        :param data_augmentation: Whenever to use data augmentation (default:
        True).
        :param fine_tunning: When fine tuning, we unfreeze pretarined model's
        last layer. (default: False.)
        """
        super().__init__()
        self.epochs = epochs
        self.validation_percent = validation_percent
        self.output_features = output_features
        self.device = device
        self.data_augmentation = data_augmentation
        self.fine_tunning = fine_tunning
        self.resize_images = resize_images

        # Proporcao entre dados de treino e de validacao
        self.train_percentage = 1 - self.validation_percent

        self.loss_function = None
        self.optimizer = None

        if self.resize_images is True:
            self.resize_tranform = transforms.Resize((224, 224))
        else:
            # If not resizing, does basically nothing (identity transform)
            self.resize_tranform = transforms.ToTensor()

        if self.data_augmentation is not True:
            self.data_transform = transforms.Compose([transforms.ToPILImage(mode="RGB"),
                                                      self.resize_tranform,
                                                      transforms.ToTensor(),
                                                      transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                                           std=[0.229, 0.224, 0.225])])
        else:
            self.data_transform = transforms.Compose([transforms.ToPILImage(mode="RGB"),
                                                      transforms.RandomHorizontalFlip(),
                                                      transforms.RandomRotation(5, resample=PIL.Image.BILINEAR),
                                                      self.resize_tranform,
                                                      transforms.ToTensor(),
                                                      transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                                           std=[0.229, 0.224, 0.225])])

        # ==Load pretrained resnet18, throw out FC and freeze all its layers====
        self.pretrained_model = torchvision.models.resnet18(pretrained=True, progress=True)
        # print(self.pretrained_model)
        self.pretrained_model = torch.nn.Sequential(*(list(self.pretrained_model.children())[:-1]))
        for name, param in self.pretrained_model.named_parameters():
            param.requires_grad = False
        # Evaluation mode for batch_normalization layers to compute correctly.
        self.pretrained_model.eval()
        # ======================================================================

        # FC layers for classification
        self.classifier = Sequential(Linear(512, 10), LeakyReLU(), Linear(10, 1))

        return

    def forward(self, input_seq):
        """
Classic forward method of every PyTorch model, as fast as possible. Receives an
input sequence and returns the prediction.

        :param input_seq: Input sequence.
        :return: The prediction itself.
        """

        # resnet18
        output = self.pretrained_model(input_seq)

        # 2 leaky_relu layers and a linear classifier
        output = self.classifier(output.view(-1, 512))

        return output

    def fit(self, X=None, y=None):
        """
This method contains customized script for training this estimator. Data is
obtained with PyTorch's dataset and dataloader classes for memory efficiency
when dealing with big datasets. Otherwise, loading the whole dataset would
overflow RAM memory. It must be adjusted whenever the network structure changes.

    :param X: Input X data as numpy array.
    :param y: Respective output for each input sequence. Also numpy array
    :return: Trained model with best validation loss found (it uses checkpoint).
        """
        X_original = X
        y_original = y
        self.train()
        self.to(self.device)
        # =====DATA-PREPARATION=================================================
        dataset = BreakhisDatasetV2(
            transform=self.data_transform,
            magnifying_factors=MAGNIFYING_FACTORS
        )

        if X is not None and y is not None:
            # Supondo que usamos SKLEARN, separamos só a parte usada neste FOLD.
            dataset = Subset(dataset, X)

        train_dataset = Subset(dataset, arange(int(len(dataset) * self.train_percentage)))
        val_dataset = Subset(dataset, arange(int(len(dataset) * self.train_percentage), len(dataset)))

        train_loader = DataLoader(train_dataset, batch_size=64, shuffle=True, num_workers=2)
        val_loader = DataLoader(val_dataset, batch_size=64, shuffle=True, num_workers=2)
        # =====fim-DATA-PREPARATION=============================================

        # class_weights = compute_class_weight("balanced", [0, 1], dataset.dataset.dataset_y)
        # class_weights = from_numpy(class_weights.astype("float32")).to(self.device)

        epochs = self.epochs
        best_validation_loss = 999999
        if self.loss_function is None: self.loss_function = nn.BCEWithLogitsLoss(weight=tensor([0.5], device=self.device).to(float32))
        if self.optimizer is None: self.optimizer = torch.optim.Adam(self.parameters(), lr=0.00005)

        f = open("loss_log.csv", "w")
        w = csv.writer(f)
        w.writerow(["epoch", "training_loss", "val_loss"])

        tqdm_bar = tqdm(range(epochs))
        for i in tqdm_bar:
            train_manager = DataManager(train_loader, device=self.device, buffer_size=1)
            val_manager = DataManager(val_loader, device=self.device, buffer_size=1)
            training_loss = 0
            validation_loss = 0
            self.optimizer.zero_grad()
            for j, (X, y) in enumerate(train_manager):
                y_pred = self(X)

                # Repare que NAO ESTAMOS acumulando a LOSS.
                single_loss = self.loss_function(y_pred, y.view(-1, self.output_features))
                # Cada chamada ao backprop eh ACUMULADA no gradiente (optimizer)
                single_loss.backward()

                # Otimizamos em batch
                self.optimizer.step()
                self.optimizer.zero_grad()

                training_loss += single_loss

            # Tira a media das losses.
            training_loss = training_loss / (j + 1)

            for j, (X, y) in enumerate(val_manager):
                y_pred = self(X)

                # Repare que NAO ESTAMOS acumulando a LOSS.
                single_loss = self.loss_function(y_pred, y.view(-1, self.output_features))

                validation_loss += single_loss

            # Tira a media das losses.
            validation_loss = validation_loss / (j + 1)

            # CHECKPOINT to best models found.
            if best_validation_loss > validation_loss:
                # Update the new best loss.
                best_validation_loss = validation_loss
                # torch.save(self, "{:.15f}".format(best_validation_loss) + "_checkpoint.pth")
                torch.save(self, "best_model.pth")
                torch.save(self.state_dict(), "best_model_state_dict.pth")

            tqdm_bar.set_description(f'epoch: {i:1} train_loss: {training_loss.item():10.10f}' + f' val_loss: {validation_loss.item():10.10f}')

            w.writerow([i, training_loss.item(), validation_loss.item()])
            f.flush()
        f.close()

        self.eval()

        # At the end of training, save the final model.
        torch.save(self, "last_training_model.pth")

        # Update itself with BEST weights foundfor each layer.
        self.load_state_dict(torch.load("best_model_state_dict.pth"))

        self.eval()

        best_model = torch.load("best_model.pth")

        if self.fine_tunning is True:
            best_model = self.fit_fine_tunning(X_original, y_original)

        # Returns the best model found so far.
        return best_model

    def fit_fine_tunning(self, X=None, y=None):
        """
This method contains customized script for training this estimator. Data is
obtained with PyTorch's dataset and dataloader classes for memory efficiency
when dealing with big datasets. Otherwise, loading the whole dataset would
overflow RAM memory. It must be adjusted whenever the network structure changes.

    :param X: Input X data as numpy array.
    :param y: Respective output for each input sequence. Also numpy array
    :return: Trained model with best validation loss found (it uses checkpoint).
        """
        self.train()
        self.to(self.device)
        # =====DATA-PREPARATION=================================================
        dataset = BreakhisDatasetV2(
            transform=self.data_transform,
            magnifying_factors=MAGNIFYING_FACTORS
        )

        if X is not None and y is not None:
            # Supondo que usamos SKLEARN, separamos só a parte usada neste FOLD.
            dataset = Subset(dataset, X)

        train_dataset = Subset(dataset, arange(int(len(dataset) * self.train_percentage)))
        val_dataset = Subset(dataset, arange(int(len(dataset) * self.train_percentage), len(dataset)))

        train_loader = DataLoader(train_dataset, batch_size=64, shuffle=True, num_workers=2)
        val_loader = DataLoader(val_dataset, batch_size=64, shuffle=True, num_workers=2)
        # =====fim-DATA-PREPARATION=============================================

        # class_weights = compute_class_weight("balanced", [0, 1], dataset.dataset.dataset_y)
        # class_weights = from_numpy(class_weights.astype("float32")).to(self.device)

        epochs = 15
        best_validation_loss = 999999
        if self.loss_function is None: self.loss_function = nn.BCEWithLogitsLoss()
        if self.optimizer is None: self.optimizer = torch.optim.Adam(self.parameters(), lr=0.000001)

        for name, param in self.pretrained_model.named_parameters():
            param.requires_grad = False
        # Evaluation mode for batch_normalization layers to compute correctly.
        self.pretrained_model.train()

        f = open("loss_log.csv", "w")
        w = csv.writer(f)
        w.writerow(["epoch", "training_loss", "val_loss"])

        tqdm_bar = tqdm(range(epochs))
        for i in tqdm_bar:
            train_manager = DataManager(train_loader, device=self.device, buffer_size=1)
            val_manager = DataManager(val_loader, device=self.device, buffer_size=1)
            training_loss = 0
            validation_loss = 0
            self.optimizer.zero_grad()
            self.pretrained_model.train()
            for j, (X, y) in enumerate(train_manager):
                y_pred = self(X)

                # Repare que NAO ESTAMOS acumulando a LOSS.
                single_loss = self.loss_function(y_pred, y.view(-1, self.output_features))
                # Cada chamada ao backprop eh ACUMULADA no gradiente (optimizer)
                single_loss.backward()

                # Otimizamos em batch
                self.optimizer.step()
                self.optimizer.zero_grad()

                training_loss += single_loss

            # Tira a media das losses.
            training_loss = training_loss / (j + 1)

            self.pretrained_model.eval()
            for j, (X, y) in enumerate(val_manager):
                y_pred = self(X)

                # Repare que NAO ESTAMOS acumulando a LOSS.
                single_loss = self.loss_function(y_pred, y.view(-1, self.output_features))

                validation_loss += single_loss

            # Tira a media das losses.
            validation_loss = validation_loss / (j + 1)

            # CHECKPOINT to best models found.
            if best_validation_loss > validation_loss:
                # Update the new best loss.
                best_validation_loss = validation_loss
                # torch.save(self, "{:.15f}".format(best_validation_loss) + "_checkpoint.pth")
                torch.save(self, "best_model.pth")
                torch.save(self.state_dict(), "best_model_state_dict.pth")

            tqdm_bar.set_description(f'epoch: {i:1} train_loss: {training_loss.item():10.10f}' + f' val_loss: {validation_loss.item():10.10f}')

            w.writerow([i, training_loss.item(), validation_loss.item()])
            f.flush()
        f.close()

        self.eval()

        # At the end of training, save the final model.
        torch.save(self, "last_training_model.pth")

        # Update itself with BEST weights foundfor each layer.
        self.load_state_dict(torch.load("best_model_state_dict.pth"))

        self.eval()

        # Returns the best model found so far.
        return torch.load("best_model.pth")

    def get_params(self, *args, **kwargs):
        """
Get parameters for this estimator.

        :param args: Always ignored, exists for compatibility.
        :param kwargs: Always ignored, exists for compatibility.
        :return: Dict containing all parameters for this estimator.
        """
        return {"epochs": self.epochs,
                "validation_percent": self.validation_percent,
                "device": self.device,
                "data_augmentation": self.data_augmentation,
                "fine_tunning": self.fine_tunning,
                "resize_images": self.resize_images}

    def predict(self, inputs, train=True):
        """
Predict using this pytorch model. Useful for sklearn search or prediction.

        :param X: Input data of shape (n_samples, n_features).
        :return: The y predicted values.
        """
        # This method (predict) is intended to be used within training procces.
        self.eval()

        data_transform = transforms.Compose([transforms.ToPILImage(mode="RGB"),
                                             self.resize_tranform,
                                             transforms.ToTensor(),
                                             transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                                  std=[0.229, 0.224, 0.225])])
        dataset = BreakhisDatasetV2(
            transform=self.data_transform,
            magnifying_factors=MAGNIFYING_FACTORS,
            train=train
        )

        y_list_aux = []

        for X in inputs:

            # We receive index and return image (first idex dataset)
            X = dataset[X][0]

            if not isinstance(X, torch.Tensor):
                X = from_numpy(X).view((1,) + X.shape)
            else:
                X = X.view((1,) + X.shape)

            y = self(X.to(self.device))

            if y.view(y.shape[1:]).detach().cpu().numpy()[0] > 0.5:
                y_list_aux.append(1.0)
            else:
                y_list_aux.append(0.0)

        return array(y_list_aux)

    def predict_proba(self, inputs, train=True):
        """
Predict using this pytorch model. Useful for sklearn search or prediction.

        :param X: Input data of shape (n_samples, n_features).
        :return: The y predicted values.
        """
        # This method (predict) is intended to be used within training procces.
        self.eval()

        data_transform = transforms.Compose([transforms.ToPILImage(mode="RGB"),
                                             self.resize_tranform,
                                             transforms.ToTensor(),
                                             transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                                  std=[0.229, 0.224, 0.225])])
        dataset = BreakhisDatasetV2(
            transform=self.data_transform,
            magnifying_factors=MAGNIFYING_FACTORS,
            train=train
        )

        y_list_aux = []

        for X in inputs:

            # We receive index and return image (first idex dataset)
            X = dataset[X][0]

            if not isinstance(X, torch.Tensor):
                X = from_numpy(X).view((1,) + X.shape)
            else:
                X = X.view((1,) + X.shape)

            y = self(X.to(self.device))

            predicao = y.view(y.shape[1:]).detach().cpu().numpy()[0]

            y_list_aux.append([1 - predicao, predicao])

        return array(y_list_aux)

    def score(self, X, y, **kwargs):
        """
Return the RMSE error score of the prediction.

        :param X: Input data os shape (n_samples, n_features).
        :param y: Predicted y values of shape (n_samples, n_outputs).)
        :param kwargs: Always ignored, exists for compatibility.
        :return: RMSE score.
        """
        # Se for um tensor, devemos converter antes para array numpy, ou o
        # sklearn retorna erro na RMSE.
        if isinstance(y, torch.Tensor):
            y = y.numpy()

        return make_scorer((mean_squared_error(self.predict(X).cpu().detach().numpy(), y)) ** 1 / 2, greater_is_better=False)

    def set_params(self, **params):
        """
Set the parameters of this estimator.

        :param params: (Dict) Estimator parameters.
        :return: Estimator instance.
        """
        epochs = params.get('epochs')
        validation_percent = params.get('validation_percent')
        device = params.get('device')
        data_augmentation = params.get('data_augmentation')
        fine_tunning = params.get('fine_tunning')
        resize_images = params.get('resize_images')

        if epochs:
            self.epochs = epochs
            self.__reinit_params__()
        if validation_percent:
            self.validation_percent = validation_percent
            self.__reinit_params__()
        if device:
            self.device = device
            self.__reinit_params__()
        if data_augmentation:
            self.data_augmentation = data_augmentation
            self.__reinit_params__()
        if fine_tunning:
            self.fine_tunning = fine_tunning
            self.__reinit_params__()
        if resize_images:
            self.resize_images = resize_images
            self.__reinit_params__()

        return self

    def __reinit_params__(self):
        """
Useful for updating params when 'set_params' is called.
        """

        self.loss_function = None
        self.optimizer = None

        if self.resize_images is True:
            self.resize_tranform = transforms.Resize((224, 224))
        else:
            # If not resizing, does basically nothing (identity transform)
            self.resize_tranform = transforms.ToTensor()

        if self.data_augmentation is True:
            self.data_transform = transforms.Compose([transforms.ToPILImage(mode="RGB"),
                                                      self.resize_tranform,
                                                      transforms.ToTensor(),
                                                      transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                                           std=[0.229, 0.224, 0.225])])
        else:
            self.data_transform = transforms.Compose([transforms.ToPILImage(mode="RGB"),
                                                      transforms.RandomHorizontalFlip(),
                                                      transforms.RandomRotation(40, resample=PIL.Image.BILINEAR),
                                                      self.resize_tranform,
                                                      transforms.ToTensor(),
                                                      transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                                           std=[0.229, 0.224, 0.225])])

        # ==Load pretrained resnet18, throw out FC and freeze all its layers====
        self.pretrained_model = torchvision.models.resnet18(pretrained=True, progress=True)
        # print(self.pretrained_model)
        self.pretrained_model = torch.nn.Sequential(*(list(self.pretrained_model.children())[:-1]))
        for name, param in self.pretrained_model.named_parameters():
            param.requires_grad = False
        # Evaluation mode for batch_normalization layers to compute correctly.
        self.pretrained_model.eval()
        # ======================================================================

        # FC layers for classification
        self.classifier = Sequential(Linear(512, 10), LeakyReLU(), Linear(10, 1))

        return


def experiment(*args, **kwargs):
    # Reproduce experiment
    random.seed(1234)
    torch.manual_seed(1234)

    model = DiagModule(epochs=1, device=device, resize_images=True)

    # Do not delete this line. Very useful for visualization of outputs shapes.
    # print(model.pretrained_model(torch.ones((1, 3, 700, 460))).shape)

    # Define X and y
    dataset = BreakhisDatasetV2(
        transform=None,
        magnifying_factors=MAGNIFYING_FACTORS,
        train=True
    )

    X = arange(len(dataset))
    y = array(dataset.labels)

    model.fit()

    # Exemplos de paramteros possiveis
    redes = ["resnet18", "vgg"]
    data_augmentation = [False]
    fine_tunning = [False]
    resize_images = [False]
    # dataset

    # Une os parametros de entrada em um unico dicionario a ser passado para a
    # funcao.
    parametros = {'data_augmentation': data_augmentation, "fine_tunning": fine_tunning, "resize_images": resize_images}

    splitter = StratifiedShuffleSplit(n_splits=1, test_size=0.3, random_state=1234)
    regressor = model
    cv_search = \
        GridSearchCV(estimator=regressor, cv=splitter,
                     param_grid=parametros,
                     refit=True,
                     verbose=1,
                     # n_jobs=4,
                     scoring=make_scorer(roc_auc_score,
                                         greater_is_better=True,
                                         needs_proba=True))

    # Let's go fit! Comment if only loading pretrained model.
    # Realizamos a busca atraves do treinamento
    cv_search.fit(X, y.reshape(-1, 1))
    # print(cv_search.cv_results_)
    cv_dataframe_results = DataFrame.from_dict(cv_search.cv_results_)
    cv_dataframe_results.to_csv("cv_results.csv")
    # display(cv_dataframe_results)

    model = cv_search.best_estimator_

    dataset = BreakhisDatasetV2(
        magnifying_factors=MAGNIFYING_FACTORS,
        train=False
    )

    X_test = arange(len(dataset))
    y_test = array(dataset.labels)

    # Avaliamos no conjunto de teste
    y_pred = model.predict(X_test, train=False)
    print("Matriz de confusão teste:\n", confusion_matrix(y_test, y_pred))

    y_prob = model.predict_proba(X_test, train=False)[:, 1]
    print("ROC AUC score teste:", roc_auc_score(y_test, y_prob))

    # Avaliamos no conjunto de treino tambem, so para fim de comparacao
    y_pred = model.predict(X, train=True)
    print("Matriz de confusão treino:\n", confusion_matrix(y, y_pred))

    y_prob = model.predict_proba(X, train=True)[:, 1]
    print("ROC AUC score treino:", roc_auc_score(y, y_prob))

    return


if __name__ == '__main__':
    if torch.cuda.is_available():
        dev = "cuda:0"
        print("Usando GPU")
    else:
        dev = "cpu"
        print("Usando CPU")
    device = torch.device(dev)

    experiment()
