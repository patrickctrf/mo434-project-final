import os
import re
import tarfile
from typing import List, Tuple

import numpy as np
from cv2 import COLOR_BGR2RGB, cvtColor, imread
from torch.utils.data import Dataset
from torchvision.transforms import Compose

try:
    import gdown

except ModuleNotFoundError as err:
    raise Exception('You should install gdown so the script '
                    'can retrive the dataset. Do it as follows:\n'
                    'pip install gdown') from err

try:
    import wget

except ModuleNotFoundError as err:
    raise Exception('You should install wget so the script '
                    'can retrive the dsfold files. Do it as follows:\n'
                    'pip install wget') from err

PATTERN = re.compile(
    r'(?P<procedure>SOB)_'
    r'(?P<class>[B|M])_'
    r'(?P<type>A|F|PT|TA|DC|LC|MC|PC)-'
    r'(?P<year>[0-9]{2})-'
    r'(?P<person>[0-9]*[A-Z]*)-'
    r'(?P<mag>40|100|200|400)-'
    r'(?P<seq>[0-9]*)'
)

TUMOR_TYPE = {
    # Benign
    'A': 'adenosis',
    'F': 'fibroadenoma',
    'PT': 'phyllodes_tumor',
    'TA': 'tubular_adenoma',

    # Malignant
    'DC': 'ductal_carcinoma',
    'LC': 'lobular_carcinoma',
    'MC': 'mucinous_carcinoma',
    'PC': 'papillary_carcinoma'
}

TUMOR_CLASS = {
    'B': 'benign',
    'M': 'malignant'
}


class BreakhisDatasetV2(Dataset):
    """
    Second version of BreaKHis Dataset interface for PyTorch

    This version does not allows custom partitioning, instead it uses the fold
    files provided by the dataset authors. See more in BreaKHis web page.

    Args:
        transform: a composition of transformations that will be applied to
            the image before returning it
        fold_path: the path to a file containing the folded images
        magnifying_factors: a list of magnifying factors that should be used
        train: if True, will use the training samples. It will use the Test
            samples otherwise
        dataset_path: path to the BreaKHis folder root
    """

    def __init__(self,
                 magnifying_factors: List[int],
                 train: bool = True,
                 fold_path: str = './dsfold4.txt',
                 dataset_path: str = './BreaKHis_v1/',
                 transform: Compose = None):
        self.dataset_path = dataset_path
        self.transform = transform

        verify_fold_file_exists(fold_file_path=fold_path)
        verify_dataset_exists(dataset_path=dataset_path)

        filenames, labels = load_fold(
            fold_path=fold_path,
            magnifying_factors=magnifying_factors,
            dataset_path=dataset_path,
            partition_name='train' if train else 'test'
        )

        self.filenames = filenames
        self.labels = labels

        self.file_paths = [
            filename_to_path(
                filename=filename,
                dataset_path=dataset_path
            )

            for filename in self.filenames
        ]

    def __len__(self):
        return len(self.filenames)

    def __getitem__(self, i: int) -> Tuple[np.array, int]:
        image = cvtColor(imread(self.file_paths[i]), COLOR_BGR2RGB) / 255.0

        if self.transform:
            image = self.transform(image)

        return image, self.labels[i]


def verify_fold_file_exists(fold_file_path: str):
    """
    Verify if the dsfold files exists and downloads it if not

    Args:
        fold_file_path: the path to the desired fold file
    """
    if os.path.exists(fold_file_path):
        print('Found dsfold on local folder')
        return

    print('Could not find the dsfold file, downloading it from source')

    url = 'https://drive.google.com/uc?id=1ri9vtKlbYBDKPygVffCSeMYsalJDIn3r'
    gdown.download(url, 'mkfold.tar.gz', quiet=False)

    tar = tarfile.open('./mkfold.tar.gz')
    tar.extractall(path='./')
    tar.close()


def verify_dataset_exists(dataset_path: str = './BreaKHis_v1/'):
    """
    Verify if the dataset already exists on the hosts or donwload it otherwise

    Args:
        dataset_path: the path to the folder where the dataset should be
            located
    """
    if os.path.exists(dataset_path):
        print('The dataset already exists on the local host.')
        return

    print('The uncompressed dataset does not exists, verifying if we can find'
          'the zipped file in the current folder')

    if not os.path.exists('./BreaKHis_v1.tar.gz'):
        print('Cannot find BreakHis_v1.tar.gz in the current folder; '
              'downloading it.\nHint: you can place the BreaKHis_v1.tar.gz '
              'in the current folder to avoid downloading it again')

        print('It will take 20 minutes...')

        url = 'https://drive.google.com/uc?id=1KF534N0HgpwzzNo9BlHzXH-LXErfFmt0'
        gdown.download(url, 'BreaKHis_v1.tar.gz', quiet=False)

    print('Uncompressing...')

    tar = tarfile.open('./BreaKHis_v1.tar.gz')
    tar.extractall(path='./')
    tar.close()


def load_fold(fold_path: str, magnifying_factors: List[int],
              dataset_path: str, partition_name: str = 'train') \
        -> Tuple[List[str], List[int]]:
    """
    Load filenames from a fold file, keeping the ones with the given
    magnifying_factors and partition_name

    Args:
        fold_path: the path to the fold file, containing the image names and
            the partition it belongs to
        magnifying_factors: a list of magnifyinf factors that should be
            included
        dataset_path: the path to the dataset root folder
        partition_name: one of `train` or `test`
    """
    filenames = []
    labels = []

    with open(fold_path) as fold_file:
        for filename in fold_file:
            match = PATTERN.match(filename)
            groups = match.groupdict()

            if int(groups['mag']) not in magnifying_factors:
                continue

            if partition_name not in filename:
                continue

            filename = filename.split('.')[0]
            filenames.append(filename)

            labels.append(1 if groups['class'] == 'M' else 0)

    return filenames, labels


def filename_to_path(filename: str, dataset_path: str) -> str:
    """
    Converts the filename to its full path in the dataset folder

    Args:
        filename: the filename, without the png extension
        dataset_path: the path to the dataset root folder
    """
    match = PATTERN.match(filename)
    group = match.groupdict()

    procedure = group['procedure']
    tumor_class = group['class']
    tumor_type = group['type']
    year = group['year']
    person = group['person']
    mag = str(group['mag']) + 'X'

    folder_name = f'{procedure}_{tumor_class}_{tumor_type}_{year}-{person}'

    return os.path.join(
        dataset_path,
        'histology_slides/breast/',
        TUMOR_CLASS[tumor_class],
        procedure,
        TUMOR_TYPE[tumor_type],
        folder_name,
        mag,
        filename + '.png'
    )
