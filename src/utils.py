import glob
import os
import shutil
from queue import Queue
from threading import Thread, Event

import torch
from numpy import load, arange, random, array, int64, absolute, asarray
from torch import from_numpy
from torch.nn.utils.rnn import pack_sequence
from torch.utils.data import Dataset


def split_into_chunks(lista, split_dims):
    """
Split a list into evenly sized chunks. The last chunk will be smaller if the
original list length is not divisible by 'split_dims'.

    :param lista: List to be split.
    :param split_dims: Length of each split chunk.
    """
    aux_list = []
    # For item i in a range that is a length of l,
    for i in range(0, len(lista), split_dims):
        # Create an index range for l of n items:
        aux_list.append(lista[i:i + split_dims])

    return aux_list


def find_nearest(array_to_search, value):
    """
This function takes 1 array as first argument and a value to find the element
in array whose value is the closest. Returns the closest value element and its
index in the original array.

    :param array_to_search: Reference array.
    :param value: Value to find closest element.
    :return: Tuple (Element value, element index).
    """
    array_to_search = asarray(array_to_search)
    idx = (absolute(array_to_search - value)).argmin()
    return array_to_search[idx], idx


class DataManager(Thread):
    def __init__(self, data_loader, buffer_size=3, device=torch.device("cpu"), data_type=torch.float32, stop_event=Event(), update_rate=1.0):
        super().__init__()
        self.buffer_queue = Queue(maxsize=buffer_size)
        self.data_loader = data_loader
        self.buffer_size = buffer_size
        self.device = device
        self.data_type = data_type
        self.stop_event = stop_event
        self.update_rate = update_rate

        self.dataloader_finished = False

    def run(self):
        for i, (x, y) in enumerate(self.data_loader):
            # Important to set before put in queue to avoid race condition
            # would happen if trying to get() in next() method before setting this flag
            if i >= len(self) - 1:
                self.dataloader_finished = True

            self.buffer_queue.put([x.to(self.data_type).to(self.device),
                                   y.to(self.data_type).to(self.device)])

    def __iter__(self):
        """
Returns an iterable of itself.

        :return: Iterable around this class.
        """
        self.start()
        self.dataloader_finished = False
        return self

    def __next__(self):
        """
Intended to be used as iterator.

        :return: Next iteration element.
        """
        if self.dataloader_finished is True and self.buffer_queue.empty():
            raise StopIteration()

        return self.buffer_queue.get()

    def __len__(self):
        return len(self.data_loader)
