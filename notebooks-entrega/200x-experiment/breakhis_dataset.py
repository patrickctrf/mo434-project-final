import glob
import time

from cv2 import imread, COLOR_BGR2RGB, cvtColor
from numpy import array, zeros, ones, hstack, vstack
from numpy.random import shuffle
from torch.utils.data import Dataset


class BreakhisDataset(Dataset):
    def __init__(self, transform=None, shuffle_data=True):
        """
PyTorch's dataset class for dealing with BreakHIS breast cancer dataset.

        :param transform: Which transforms to apply.
        :param shuffle_data: Whenever to shuffle data in initialization.
        """
        self.transform = transform

        benignos_array = array(glob.glob("breakhis/benignos/*.png"))
        malignos_array = array(glob.glob("breakhis/malignos/*.png"))

        zeros_array = zeros(benignos_array.shape)
        ones_array = ones(malignos_array.shape)

        dataset_array = vstack((hstack((benignos_array, malignos_array)), hstack((zeros_array, ones_array)))).T

        if shuffle_data is True:
            shuffle(dataset_array)

        self.dataset_x = dataset_array[:, 0]
        self.dataset_y = dataset_array[:, 1].astype("float32")

        # avoid "if" conditional in __getitem__
        if self.transform is None:
            self.transform = lambda x: x

        return

    def __len__(self):
        return self.dataset_x.shape[0]

    def __getitem__(self, i):
        return self.transform(cvtColor(imread(self.dataset_x[i]), COLOR_BGR2RGB)/255.0), self.dataset_y[i]
